package org.firstinspires.ftc.teamcode.utils;

public class PIDController {

    private double kp, ki, kd;
    private double minOut, maxOut;
    private double lastError, lastTime;
    private double intError;
    private double error;

    public PIDController(double kProp, double kInt, double kDeriv, double minOutput, double maxOutput) {
        this.kp = kProp;
        this.ki = kInt;
        this.kd = kDeriv;
        this.minOut = minOutput;
        this.maxOut = maxOutput;
        lastError = 0;
        lastTime = 0;
        intError = 0;
    }

    public double output(double current, double target, double time) {
        error = current - target;

        double de = error - lastError;
        double dt = time - lastTime;

        intError = lastTime == 0 ? 0 : intError +(error * dt);

        double p = kp * error;
        double i = ki * intError;
        double d = kd * (de / dt);

        double u = Math.min(maxOut, Math.max(minOut, p + i + d));
        lastError = error;
        lastTime = time;
        return u;
    }

    public void setCoefficients(double kpNew, double kiNew, double kdNew) {
        this.kp = kpNew;
        this.ki = kiNew;
        this.kd = kdNew;
    }

    // For testing purposes
    public double getError() {
        return error;
    }
}
