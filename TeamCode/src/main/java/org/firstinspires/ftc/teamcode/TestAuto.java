package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.teamcode.utils.BotcatsLog;
import org.firstinspires.ftc.teamcode.utils.Coordinates;

import static org.firstinspires.ftc.teamcode.Autonomous.StartPosition.CRATER;
import static org.firstinspires.ftc.teamcode.Autonomous.Team.BLUE;
import static org.firstinspires.ftc.teamcode.BaseOpMode.OpModeType.AUTONOMOUS;

@com.qualcomm.robotcore.eventloop.opmode.Autonomous(name="Test Autonomous", group="Test")
public class TestAuto extends Autonomous {



    @Override
    public void runOpMode() throws InterruptedException {

        type = AUTONOMOUS;

        runtime = new ElapsedTime();

        debugLog = new BotcatsLog(runtime, "Test Autonomous", BotcatsLog.LogType.DEBUG);
        positionLog = new BotcatsLog(runtime, "Position", BotcatsLog.LogType.DATA);
        debugLog.openLog();
        positionLog.openLog();

        initialize();

        telemetry.addData("Status", "Initialized");
        telemetry.update();

        if (loggingEnabled) {
            debugLog.addDbgMessage(
                    BotcatsLog.DbgLevel.INFO,
                    "OpMode",
                    "Starting Angle: " + imu.getHeading()
            );
        }
        tts.lang = tts.randomLang();

        waitForStart();

        tts.setLanguage();
        tts.speak(tts.welcomeText());

        if (loggingEnabled) {
            debugLog.addDbgMessage(
                    BotcatsLog.DbgLevel.INFO,
                    "OpMode",
                    "---------- Starting Autonomous ----------"
            );
        }

        runtime.reset();
        lift.extend();
        sleep(500);
        drive.goDistance(20, 0.5);
        sleep(1000);
        positionLog.closeLog();
        shutdown();
    }

    @Override
    void setAutoVars() {
        team = BLUE;
        startPosition = CRATER;
    }

}
