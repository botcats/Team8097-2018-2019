package org.firstinspires.ftc.teamcode.utils;

import org.firstinspires.ftc.teamcode.BaseOpMode;
import org.firstinspires.ftc.teamcode.utils.BotcatsLog;

public class Module {

    public boolean moduleLoggingEnabled;
    public String moduleName;
    public BaseOpMode op;

    public Module() {
        try {
            op = BaseOpMode.getInstance();
            moduleLoggingEnabled = true;
        } catch (NullPointerException e) {
            moduleLoggingEnabled = false;
        }
    }
}
