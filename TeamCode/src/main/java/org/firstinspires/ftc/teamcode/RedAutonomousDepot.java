package org.firstinspires.ftc.teamcode;

import static org.firstinspires.ftc.teamcode.Autonomous.StartPosition.DEPOT;
import static org.firstinspires.ftc.teamcode.Autonomous.Team.RED;

@com.qualcomm.robotcore.eventloop.opmode.Autonomous(name="Red Auto (Depot)", group ="Autonomous")
public class RedAutonomousDepot extends Autonomous {

    @Override
    void setAutoVars() {
        team = RED;
        startPosition = DEPOT;
        angleOffset = 45;
        xOffset = xyOffset;
        yOffset = xyOffset;
    }
}
