package org.firstinspires.ftc.teamcode.hardware;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;

import org.firstinspires.ftc.teamcode.utils.BotcatsLog;

import java.util.ArrayList;

public class TankDrive extends Drive {

    public TankDrive(ArrayList<DcMotor> leftMotors, ArrayList<DcMotor> rightMotors) {
        super();

        this.leftMotors = leftMotors;
        this.rightMotors = rightMotors;
        moduleName = "Tank Drive";

        for (int i = 0; i < leftMotors.size(); i++) {
            if (leftMotors.get(i) == null) {
                if (moduleLoggingEnabled) {
                    op.debugLog.addDbgMessage(
                            BotcatsLog.DbgLevel.ERROR,
                            moduleName,
                            "Left motor of index " + i + " not found!"
                    );
                }
                leftMotors.remove(i);
                rightMotors.remove(i);
            }
        }
        for (int i = 0; i < rightMotors.size(); i++) {
            if (rightMotors.get(i) == null) {
                if (moduleLoggingEnabled) {
                    op.debugLog.addDbgMessage(
                            BotcatsLog.DbgLevel.ERROR,
                            moduleName,
                            "Right motor of index " + i + " not found!"
                    );
                }
                rightMotors.remove(i);
                leftMotors.remove(i);
            }
        }

        if (leftMotors.size() > 0 && rightMotors.size() > 0) {
            if (op.imuEnabled) {
                imu = op.imu;
            }

            if (op.coordinatesEnabled) {
                robotCoords = op.robotCoords;
            }

            wheelCirc = HardwareConstants.CIRC_STEALTH_4; // Defaults to the circumference of 4" stealth wheels

            allMotors = new ArrayList<>();
            allMotors.addAll(leftMotors);
            allMotors.addAll(rightMotors);

            for (DcMotor motor : allMotors) {
                motor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
            }

            setDirection(rightMotors, DcMotorSimple.Direction.REVERSE);
            resetEncoders(allMotors);

            if (moduleLoggingEnabled) {
                op.debugLog.addDbgMessage(
                        BotcatsLog.DbgLevel.INFO,
                        moduleName,
                        "Initialized"
                );
            }

            op.modules.add(this);

        } else {
            if (moduleLoggingEnabled) {
                op.debugLog.addDbgMessage(
                        BotcatsLog.DbgLevel.INFO,
                        moduleName,
                        "Not enough functional wheels, disabling drive"
                );
            }
            op.driveEnabled = false;
        }
    }
}
