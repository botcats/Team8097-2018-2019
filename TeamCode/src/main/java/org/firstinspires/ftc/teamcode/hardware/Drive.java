package org.firstinspires.ftc.teamcode.hardware;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.Range;
import com.qualcomm.robotcore.util.ReadWriteFile;

import org.firstinspires.ftc.robotcore.internal.collections.SimpleGson;
import org.firstinspires.ftc.robotcore.internal.system.AppUtil;
import org.firstinspires.ftc.teamcode.BaseOpMode;
import org.firstinspires.ftc.teamcode.utils.BotcatsLog;
import org.firstinspires.ftc.teamcode.utils.Coordinates;
import org.firstinspires.ftc.teamcode.utils.Module;
import org.firstinspires.ftc.teamcode.utils.PIDController;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.Array;
import java.util.ArrayList;

import static org.firstinspires.ftc.teamcode.hardware.HardwareConstants.KD_DRIVE_ANGLE;
import static org.firstinspires.ftc.teamcode.hardware.HardwareConstants.KD_DRIVE_SPEED;
import static org.firstinspires.ftc.teamcode.hardware.HardwareConstants.KD_TURN;
import static org.firstinspires.ftc.teamcode.hardware.HardwareConstants.KI_DRIVE_ANGLE;
import static org.firstinspires.ftc.teamcode.hardware.HardwareConstants.KI_DRIVE_SPEED;
import static org.firstinspires.ftc.teamcode.hardware.HardwareConstants.KI_TURN;
import static org.firstinspires.ftc.teamcode.hardware.HardwareConstants.KP_DRIVE_ANGLE;
import static org.firstinspires.ftc.teamcode.hardware.HardwareConstants.KP_DRIVE_SPEED;
import static org.firstinspires.ftc.teamcode.hardware.HardwareConstants.KP_TURN;

public class Drive extends Module {

    private final float SPEED_INCREMENT = 0.05f;
    private final float SPEED_DECREMENT = 0.05f;
    private final float SPEED_TOLERANCE = 0.05f;

    private final double ANGLE_TOLERANCE = 1.25;
    private final int TICK_TOLERANCE = 40;

    double lastAccel;

    double wheelCirc = HardwareConstants.CIRC_STEALTH_4;
    double motorTicksPerRev = HardwareConstants.TICKS_PER_REV_NEVEREST_60;

    /* Default PID Coefficients, unless otherwise loaded.
    kpTurn, kiTurn, kdTurn
    kpDriveAngle, kiDriveAngle, kdDriveAngle
    kpDriveSpeed, kiDriveSpeed, kdDriveSpeed */
    double[][] pidCoefficients = {
            {KP_TURN, KI_TURN, KD_TURN},
            {KP_DRIVE_ANGLE, KI_DRIVE_ANGLE, KD_DRIVE_ANGLE},
            {KP_DRIVE_SPEED, KI_DRIVE_SPEED, KD_DRIVE_SPEED}
    };


    RevImu imu = null;
    RevSensors revSensors = null;
    Coordinates robotCoords = null;

    public ArrayList<DcMotor> allMotors;
    public ArrayList<DcMotor> leftMotors;
    public ArrayList<DcMotor> rightMotors;

    public Drive () {
        super();
    }

    public void curveDrive(double magnitude, double inputLeft, double inputRight) {
        if (op.driveEnabled) {
            double turn = inputLeft - inputRight;
            for (DcMotor motor : leftMotors) {
                motor.setPower(-Range.clip(magnitude + turn, -1, 1));
            }
            for (DcMotor motor : rightMotors) {
                motor.setPower(-Range.clip(magnitude - turn, -1, 1));
            }
        }

    }

    public void turnLeft(double targetPower) {
        if (op.driveEnabled) {
            for (DcMotor motor : leftMotors) {
                motor.setPower(-targetPower);
            }
            for (DcMotor motor : rightMotors) {
                motor.setPower(targetPower);
            }
        }
    }

    public void turnRight(double targetPower) {
        if (op.driveEnabled) {
            setPower(leftMotors, targetPower);
            setPower(rightMotors, -targetPower);
        }
    }

    public void turnRightFromCurrent(double angle, double targetPower) {
        if (op.driveEnabled && op.imuEnabled) {
            double turnAngle = imu.getHeading() - Math.abs(angle);
            turnTo(turnAngle, targetPower);
        } else if (op.driveEnabled && !op.imuEnabled) {
            if (moduleLoggingEnabled) {
                op.debugLog.addDbgMessage(
                        BotcatsLog.DbgLevel.WARN,
                        moduleName,
                        "IMU disabled or inaccessible, skipping turn"
                );
            }
        }
    }

    public void turnLeftFromCurrent(double angle, double targetPower) {
        if (op.driveEnabled && op.imuEnabled) {
            double turnAngle = imu.getHeading() + Math.abs(angle);
            turnTo(turnAngle, targetPower);
        } else if (op.driveEnabled && !op.imuEnabled) {
            if (moduleLoggingEnabled) {
                op.debugLog.addDbgMessage(
                        BotcatsLog.DbgLevel.WARN,
                        moduleName,
                        "IMU disabled or inaccessible, skipping turn"
                );
            }
        }
    }

    // Autonomous Functions

    public void turnTo(double targetHeading, double power, double tolerance) {
        if (op.driveEnabled && op.imuEnabled && op.opModeIsActive()) {
            resetEncoders(allMotors);
            PIDController turnController = new PIDController(
                    pidCoefficients[0][0], pidCoefficients[0][1], pidCoefficients[0][2],
                    -1, 1
            );

            if (moduleLoggingEnabled) {
                op.debugLog.addDbgMessage(
                        BotcatsLog.DbgLevel.INFO,
                        moduleName,
                        "Turning to angle " + targetHeading + "from " +
                                imu.getHeading() + " with power " + power
                );
            }

            while (Math.abs(imu.getHeading() - targetHeading) > tolerance
                    && op.opModeIsActive()) {
                double currentHeading = imu.getHeading();
                double out = turnController.output(currentHeading, targetHeading, op.time);
                setPower(leftMotors, out);
                setPower(rightMotors, -out);
                op.telemetry.addData("Output: ", out);
                op.telemetry.update();
            }
            setPower(leftMotors, 0);
            setPower(rightMotors, 0);
            resetEncoders(allMotors);

        } else if (op.driveEnabled && !op.imuEnabled) {
            if (moduleLoggingEnabled) {
                op.debugLog.addDbgMessage(
                        BotcatsLog.DbgLevel.WARN,
                        moduleName,
                        "IMU disabled or inaccessible, skipping turn"
                );
            }
        }
    }

    public void turnTo(double targetHeading, double power) {
        turnTo(targetHeading, power, ANGLE_TOLERANCE);
    }

    public void goDistance(double distance, double targetPower) throws InterruptedException{
        if (op.driveEnabled && op.opModeIsActive()) {
            resetEncoders(allMotors);
            int targetTicks = getTargetTicks(distance);
            double lastTicks = currentMeanTicks();
            double initialHeading;

            PIDController driveAngleController = new PIDController(
                    pidCoefficients[1][0], pidCoefficients[1][1], pidCoefficients[1][2],
                    -1, 1
            );
            PIDController driveSpeedController = new PIDController(
                    pidCoefficients[2][0], pidCoefficients[2][1], pidCoefficients[2][2],
                    -targetPower, targetPower
            );

            if (op.imuEnabled) {
                initialHeading = imu.getHeading();
            } else {
                initialHeading = 0;
            }

            if (moduleLoggingEnabled) {
                op.debugLog.addDbgMessage(
                        BotcatsLog.DbgLevel.INFO,
                        moduleName,
                        "Moving: " + distance + " inches with a speed of " + targetPower
                );
            }

            while (Math.abs(targetTicks - currentMeanTicks()) > TICK_TOLERANCE && op.opModeIsActive()) {
                double currentTicks = currentMeanTicks();
                double adjustedPower = -driveSpeedController.output(currentTicks, targetTicks, op.time);
                if (op.imuEnabled ) {
                    double currentHeading = imu.getHeading();
                    double out = -driveAngleController.output(currentHeading, initialHeading, op.time);
                    if (op.coordinatesEnabled && op.getType() == BaseOpMode.OpModeType.AUTONOMOUS) {
                        op.robotCoords.addVectorToPos(currentHeading, getDistance(currentTicks - lastTicks));
                        op.positionLog.addData(robotCoords.getX(), robotCoords.getY());
                    }
                    if (checkCollision(imu.getAccelMag(), 5)) break;
                    setPower(leftMotors, (adjustedPower - out));
                    setPower(rightMotors, (adjustedPower + out));
                } else {
                    setPower(allMotors, adjustedPower);
                }
                lastTicks = currentTicks;
            }
            setPower(leftMotors, 0);
            setPower(rightMotors, 0);
            resetEncoders(allMotors);
        }
    }

    boolean checkCollision(double accelMag, float collDelta) {
        if (Math.abs(accelMag - lastAccel) >  collDelta) {
            lastAccel = 0;
            return true;
        } else {
            lastAccel = accelMag;
            return false;
        }
    }

    public void moveToCoordinates(Coordinates targetCoordinates, double targetPower) throws InterruptedException {
        if (op.driveEnabled && op.coordinatesEnabled && op.imuEnabled) {
            double magnitude = Coordinates.getMagnitude(robotCoords, targetCoordinates);
            double direction = Coordinates.getAngle(robotCoords, targetCoordinates);

            if (direction < 0 && imu.getHeading() > 0) {
                direction += 360;
            } else if (direction > 0 && imu.getHeading() < 0) {
                direction -= 360;
            }

            if (moduleLoggingEnabled) {
                op.debugLog.addDbgMessage(
                        BotcatsLog.DbgLevel.INFO,
                        moduleName,
                        "Moving to coordinates " + targetCoordinates.getX() + ", " +
                                targetCoordinates.getY() + " from "  + robotCoords.getX() + ", " +
                                robotCoords.getY()
                );
            }
            turnTo(direction, targetPower);
            goDistance(magnitude, targetPower);
        } else if (op.driveEnabled && !op.coordinatesEnabled) {
            if (moduleLoggingEnabled) {
                op.debugLog.addDbgMessage(
                        BotcatsLog.DbgLevel.WARN,
                        moduleName,
                        "Coordinates disabled or inaccessible, skipping coordinate move"
                );
            }
        }
    }

    void resetEncoders(ArrayList<DcMotor> motors) {
        if (op.driveEnabled) {
            for (DcMotor motor : motors) {
                if (motor.getCurrentPosition() != 0) {
                    motor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                }
                motor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
            }
            if (moduleLoggingEnabled) {
                op.debugLog.addDbgMessage(
                        BotcatsLog.DbgLevel.INFO,
                        moduleName,
                        "Resetting encoders"
                );
            }
        }
    }

    private int getTargetTicks(double distance) {
        double ticks = distance * (motorTicksPerRev / wheelCirc);
        return ((int)ticks);
    }

    private double getDistance(double ticks) {
        double distance = (ticks * wheelCirc) / motorTicksPerRev;
        return distance;
    }

    private void setMode(ArrayList<DcMotor> motors, DcMotor.RunMode mode) {
        if (op.driveEnabled) {
            for (DcMotor motor : motors) motor.setMode(mode);
        }
    }

    public void setPower(ArrayList<DcMotor> motors, double power) {
        if (op.driveEnabled) {
            for (DcMotor motor : motors) motor.setPower(power);
        }
    }

    private void setTarget(ArrayList<DcMotor> motors, int targetPosition) {
        if (op.driveEnabled) {
            for (DcMotor motor : motors) motor.setTargetPosition(targetPosition);
        }
    }

    void setDirection(ArrayList<DcMotor> motors, DcMotor.Direction direction) {
        if (op.driveEnabled) {
            for (DcMotor motor : motors) motor.setDirection(direction);
        }
    }

    public void stopAll() {
        if (op.driveEnabled) {
            for (DcMotor motor : allMotors) {
                motor.setPower(0);
                motor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
            }
        }
    }

    public void gentleStopAll() {
        if (op.driveEnabled) {
            for (DcMotor motor : allMotors) {
                motor.setPower(speed(0));
                motor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
            }
        }
    }

    public void setMotorTicksPerRev() {
        if (op.driveEnabled) {
            double totalTicks = 0;
            for (DcMotor motor : allMotors) {
                totalTicks += motor.getMotorType().getTicksPerRev();
            }
            double averageTicks = totalTicks / allMotors.size();
            motorTicksPerRev = averageTicks;
        }
    }

    public void setWheels(double wheelCircumference) {
        if (op.driveEnabled) {
            wheelCirc = wheelCircumference;
        }
    }

    public void setPidCoefficients(double[][] coefficients) {
        if (op.driveEnabled) {
            if (coefficients.length == 2 && coefficients[0].length == 3 &&
                    coefficients[1].length == 3) {
                pidCoefficients = coefficients;
            } else {
                if (moduleLoggingEnabled) {
                    op.debugLog.addDbgMessage(
                            BotcatsLog.DbgLevel.WARN,
                            moduleName,
                            "Invalid PID Coefficients supplied"
                    );
                }
            }
        }
    }

    double speed(double target) {
        if (op.driveEnabled) {
            double power;

            if (target < 0) {
                if (currentMeanSpeed() > target + SPEED_TOLERANCE) {
                    power = currentMeanSpeed() - SPEED_INCREMENT;
                } else if (currentMeanSpeed() < target - SPEED_TOLERANCE) {
                    power = currentMeanSpeed() + SPEED_DECREMENT;
                } else {
                    power = target;
                }
            } else if (target > 0) {
                if (currentMeanSpeed() < target - SPEED_TOLERANCE) {
                    power = currentMeanSpeed() + SPEED_INCREMENT;
                } else if (currentMeanSpeed() > target + SPEED_TOLERANCE) {
                    power = currentMeanSpeed() - SPEED_DECREMENT;
                } else {
                    power = target;
                }
            } else {
                if (currentMeanSpeed() > 0.1) {
                    power = currentMeanSpeed() - SPEED_DECREMENT;
                } else if (currentMeanSpeed() < -.1) {
                    power = currentMeanSpeed() + SPEED_DECREMENT;
                } else {
                    power = 0;
                }
            }

            return power;
        } else {
            return 0;
        }
    }

    double totalPowerDraw() {
        if (op.driveEnabled && op.revSensorsEnabled) {
            double totalPower = 0;
            for (DcMotor motor : allMotors) {
                totalPower += revSensors.getMotorDraw(motor.getPortNumber());
            }
            return totalPower;
        } else if (op.driveEnabled && !op.revSensorsEnabled) {
            if (moduleLoggingEnabled) {
                op.debugLog.addDbgMessage(
                        BotcatsLog.DbgLevel.WARN,
                        moduleName,
                        "Rev sensors disabled or inaccessible, skipping draw measurement"
                );
            }
            return 0;
        }
        return 0;
    }

    double currentMeanTicks() {
        if (op.driveEnabled) {
            double leftTotal = 0;
            double rightTotal = 0;
            double average;
            int totalMotors;

            for (DcMotor motor : leftMotors) leftTotal += motor.getCurrentPosition();
            for (DcMotor motor : rightMotors) rightTotal += motor.getCurrentPosition();

            totalMotors = leftMotors.size() + rightMotors.size();

            average = (leftTotal + rightTotal) / totalMotors;

            return average;
        } else {
            return 0;
        }
    }

    double currentMeanSpeed() {
        if (op.driveEnabled) {
            double leftTotal = 0;
            double rightTotal = 0;
            double average;
            int totalMotors;

            for (DcMotor motor : leftMotors) leftTotal += motor.getPower();
            for (DcMotor motor : rightMotors) rightTotal += motor.getPower();

            totalMotors = leftMotors.size() + rightMotors.size();

            average = (leftTotal + rightTotal) / totalMotors;

            return average;
        } else {
            return 0;
        }
    }

    public static double[][] loadPidCoefficients() {
        String filename = "PID_Coefficients.json";
        File file = AppUtil.getInstance().getSettingsFile(filename);
        String coeffsSerial = ReadWriteFile.readFile(file);
        double[][] coefficients = SimpleGson.getInstance().fromJson(coeffsSerial, double[][].class);
        return coefficients;
    }

}
