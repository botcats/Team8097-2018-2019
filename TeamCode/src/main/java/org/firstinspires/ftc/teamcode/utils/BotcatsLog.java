package org.firstinspires.ftc.teamcode.utils;

import android.util.Log;

import com.qualcomm.robotcore.util.ElapsedTime;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class BotcatsLog {

    private PrintStream log = null;
    private LogType type;

    private boolean collectionStarted = false;

    private final String PATH = "/sdcard/Logs";
    private String name;

    private double lastTime = 0;
    private ElapsedTime time;

    public BotcatsLog(ElapsedTime opmodeTime, String name, LogType type) {
        this.type = type;
        this.name = name;
        this.time = opmodeTime;
    }

    public BotcatsLog() {
    }

    String logFileName() {
        String logName, extension, prefix;
        switch (type) {
            case DATA:
                extension = ".csv";
                prefix = "DATA";
                break;
            case DEBUG:
                extension = ".log";
                prefix = "DEBUG";
                break;
            default:
                extension = ".txt";
                prefix = "LOG";
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd@HH:mm", Locale.US);
        logName = PATH + "/" + prefix + "_" + name.toUpperCase() + "_" + dateFormat.format(new Date()) + extension;
        return logName;
    }

    public void startCollection() {
        if (!this.collectionStarted) {
            this.collectionStarted = true;
        }
    }

    public void stopCollection() {
        if (this.collectionStarted) {
            this.collectionStarted = false;
        }
    }

    public void openLog() {
        try {
            log = new PrintStream(new File(logFileName()));
        } catch (FileNotFoundException e) {
            Log.e("Logger", "Could not open log file: " + logFileName());
            log = null;
        }
    }

    public void closeLog() {
        if (log != null) {
            log.close();
            log = null;
        }
    }

    public void addData(double time, Object ... values) {
        try {
                StringBuilder dataString = dataFormat(time, values);
                log.append(dataString).append("\n");
        } catch (NullPointerException e) {
            Log.e("Logger", "Data file nonexistent!");
        }
    }

    public void addDbgMessage(DbgLevel lvl, String title, String message) {
        StringBuilder dbgString = dbgFormat(title, message);

        switch (lvl) {
            case ERROR:
                Log.e(name, dbgString.toString());
                break;
            case WARN:
                Log.e(name, dbgString.toString());
                break;
            case INFO:
                Log.i(name, dbgString.toString());
                break;
            case DEBUG:
                Log.d(name, dbgString.toString());
                break;
            }

        try {
            double truncated = getTruncatedTime();
            log.append(String.valueOf(truncated)).append("\t");
            log.append(lvl.toString()).append("\t");
            log.append(dbgString);
        } catch (NullPointerException e) {
            Log.e("Logger", "Log file nonexistent!");
        }
    }

    private double getTruncatedTime() {
        if (this.time == null) {
            return 0;
        }
        else {
            double seconds = this.time.seconds();
            double truncated = Math.floor(seconds * 10) / 10;
            return truncated;
        }
    }

    private StringBuilder dbgFormat(String function, String message) {
        StringBuilder formatted = new StringBuilder();
        formatted.append(function).append(": ");
        formatted.append(message).append("\n");
        return formatted;
    }

    private StringBuilder dataFormat(double time, Object ... values) {
        StringBuilder formatted = new StringBuilder();
        formatted.append(time);
        for (Object val : values) {
            formatted.append(",").append(val);
        }
        return formatted;
    }

    public enum LogType {
        DATA,
        DEBUG
    }

    public enum DbgLevel {
        ERROR,
        WARN,
        INFO,
        DEBUG
    }
}
