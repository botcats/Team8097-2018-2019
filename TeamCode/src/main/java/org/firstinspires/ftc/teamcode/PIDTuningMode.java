package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.util.ElapsedTime;
import com.qualcomm.robotcore.util.ReadWriteFile;

import org.firstinspires.ftc.robotcore.internal.collections.SimpleGson;
import org.firstinspires.ftc.robotcore.internal.system.AppUtil;
import org.firstinspires.ftc.teamcode.hardware.Drive;
import org.firstinspires.ftc.teamcode.utils.BotcatsLog;
import org.firstinspires.ftc.teamcode.utils.PIDController;

import java.io.File;
import java.io.OutputStreamWriter;

import static org.firstinspires.ftc.teamcode.BaseOpMode.OpModeType.TEST;
import static org.firstinspires.ftc.teamcode.hardware.HardwareConstants.KD_DRIVE_ANGLE;
import static org.firstinspires.ftc.teamcode.hardware.HardwareConstants.KD_DRIVE_SPEED;
import static org.firstinspires.ftc.teamcode.hardware.HardwareConstants.KD_TURN;
import static org.firstinspires.ftc.teamcode.hardware.HardwareConstants.KI_DRIVE_ANGLE;
import static org.firstinspires.ftc.teamcode.hardware.HardwareConstants.KI_DRIVE_SPEED;
import static org.firstinspires.ftc.teamcode.hardware.HardwareConstants.KI_TURN;
import static org.firstinspires.ftc.teamcode.hardware.HardwareConstants.KP_DRIVE_ANGLE;
import static org.firstinspires.ftc.teamcode.hardware.HardwareConstants.KP_DRIVE_SPEED;
import static org.firstinspires.ftc.teamcode.hardware.HardwareConstants.KP_TURN;

@com.qualcomm.robotcore.eventloop.opmode.TeleOp(name="PID Tuning Mode", group="Test")


public class PIDTuningMode extends BaseOpMode {

    private ElapsedTime cooldown;
    private boolean selectMode = true;
    private int x = 0;
    private int y = 0;
    private double buttonACooldown, buttonA2Cooldown, buttonYCooldown, buttonXCooldown,
            buttonBCooldown, buttonLBCooldown, buttonRBCooldown, buttonDLeftCooldown,
            buttonDRightCooldown, buttonDUpCooldown, buttonDDownCooldown;

    volatile double[][] pidCoefficients = Drive.loadPidCoefficients();

    private volatile Thread controlThread;

    public void runOpMode() throws InterruptedException {

        type = TEST;

        runtime = new ElapsedTime();
        cooldown = new ElapsedTime();

        debugLog = new BotcatsLog(runtime, "PID Tune Op", BotcatsLog.LogType.DEBUG);
        debugLog.openLog();

        initialize();

        telemetry.addData("Modules Loaded: ", modules);
        telemetry.addData("Loggers Running: ", loggers);
        telemetry.update();

        controlThread = new Thread(() -> {
            while (!Thread.currentThread().isInterrupted() && opModeIsActive()) {
                if (gamepad1.x && Math.abs(cooldown.time() - buttonXCooldown) >= .2) {
                    try {
                        drive.goDistance(100, .75);
                    } catch (InterruptedException e) {

                    }
                    buttonXCooldown = cooldown.time();
                }
                if (gamepad1.y && Math.abs(cooldown.time() - buttonYCooldown) >= .2) {
                    try {
                        drive.goDistance(-100, .75);
                    } catch (InterruptedException e) {

                    }
                    buttonYCooldown = cooldown.time();
                }


                if (gamepad1.b && Math.abs(cooldown.time() - buttonBCooldown) >= .2) {
                    PIDController turnController = new PIDController(
                            pidCoefficients[0][0], pidCoefficients[0][1], pidCoefficients[0][2],
                            -1, 1
                    );
                    while (!gamepad1.back) {
                        turnTo(45, 0.1, turnController);
                    }
                    buttonBCooldown = cooldown.time();
                }
            }
        });

        waitForStart();

        runtime.reset();

        controlThread.start();

        while (opModeIsActive()) {

            if (selectMode) {
                if (gamepad1.dpad_up && Math.abs(cooldown.time() - buttonDUpCooldown) >= .2) {
                    y--;
                    y = (y < 0) ? pidCoefficients[x].length - 1 : y;
                    buttonDUpCooldown = cooldown.time();
                } else if (gamepad1.dpad_down && Math.abs(cooldown.time() - buttonDDownCooldown) >= .2) {
                    y++;
                    y = (y > pidCoefficients[x].length - 1) ? 0 : y;
                    buttonDDownCooldown = cooldown.time();
                } else if (gamepad1.dpad_left && Math.abs(cooldown.time() - buttonDLeftCooldown) >= .2) {
                    x--;
                    x = (x < 0) ? pidCoefficients[y].length - 1 : x;
                    buttonDLeftCooldown = cooldown.time();
                } else if (gamepad1.dpad_right && Math.abs(cooldown.time() - buttonDRightCooldown) >= .2) {
                    x++;
                    x = (x > pidCoefficients[y].length - 1) ? 0 : x;
                    buttonDRightCooldown = cooldown.time();
                }
            } else {
                if (gamepad1.dpad_left && Math.abs(cooldown.time() - buttonDLeftCooldown) >= .2) {
                    pidCoefficients[y][x] -= 0.0001;
                    buttonDLeftCooldown = cooldown.time();
                }
                else if (gamepad1.dpad_right && Math.abs(cooldown.time() - buttonDRightCooldown) >= .2) {
                    pidCoefficients[y][x] += 0.0001;
                    buttonDRightCooldown = cooldown.time();
                }
            }

            if (gamepad1.a && Math.abs(cooldown.time() - buttonACooldown) >= .2) {
                selectMode = !selectMode;
                buttonACooldown = cooldown.time();
            }

            if (gamepad1.left_bumper && gamepad1.right_bumper) {
                saveCoefficients(pidCoefficients);
            }

            drive.setPidCoefficients(pidCoefficients);

            buildTelemetry();
            telemetry.update();
            if (isStopRequested()) {
                shutdown();
            }
        }
    }

    private void buildTelemetry() {
        telemetry.addLine("Press X to Move, B to Turn");
        String[][] coeffNames = {
                {"KP Turn", "KI Turn", "KD Turn"},
                {"KP Angle", "KI Angle", "KD Angle"},
                {"KD Speed", "KI Speed", "KD Speed"}
        };
        if (selectMode) {
            coeffNames[y][x] = ">" + coeffNames[y][x];
            telemetry.addLine("Press A to select");
            telemetry.addLine(coeffNames[0][0] + " " + coeffNames[0][1] + " " + coeffNames[0][2]);
            telemetry.addLine(coeffNames[1][0] + " " + coeffNames[1][1] + " " + coeffNames[1][2]);
            telemetry.addLine(coeffNames[2][0] + " " + coeffNames[2][1] + " " + coeffNames[2][2]);
        } else {
            telemetry.addLine("Press A to finish");
            telemetry.addLine(coeffNames[y][x] + ": " + pidCoefficients[y][x]);
        }
    }

    private void saveCoefficients(double[][] coefficients) {
        telemetry.clearAll();
        telemetry.update();
        String filename = "PID_Coefficients.json";
        File file = AppUtil.getInstance().getSettingsFile(filename);
        ReadWriteFile.writeFile(file, SimpleGson.getInstance().toJson(coefficients));
        telemetry.clearAll();
        telemetry.addLine("Saved PID coefficients to " + filename);
        telemetry.update();
        sleep(1500);
    }

    private void turnTo(double targetAngle, double tolerance, PIDController turnController) {
        while (Math.abs(imu.getHeading() - targetAngle) > tolerance
                && opModeIsActive()) {
            double currentHeading = imu.getHeading();
            double out = turnController.output(currentHeading, targetAngle, time);
            turnController.setCoefficients(pidCoefficients[1][0],
                    pidCoefficients[1][1], pidCoefficients[1][2]);
            drive.setPower(drive.leftMotors, out);
            drive.setPower(drive.rightMotors, -out);
        }
        drive.setPower(drive.leftMotors, 0);
        drive.setPower(drive.rightMotors, 0);
    }
}
