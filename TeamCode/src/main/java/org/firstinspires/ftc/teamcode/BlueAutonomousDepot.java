package org.firstinspires.ftc.teamcode;

import static org.firstinspires.ftc.teamcode.Autonomous.StartPosition.DEPOT;
import static org.firstinspires.ftc.teamcode.Autonomous.Team.BLUE;

@com.qualcomm.robotcore.eventloop.opmode.Autonomous(name="Blue Auto (Depot)", group ="Autonomous")
public class BlueAutonomousDepot extends Autonomous {

    @Override
    void setAutoVars() {
        team = BLUE;
        startPosition = DEPOT;
        angleOffset = 225;
        xOffset = -xyOffset;
        yOffset = -xyOffset;
    }
}
