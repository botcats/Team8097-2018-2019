package org.firstinspires.ftc.teamcode.utils;

public class Coordinates{

    private double xPos;
    private double yPos;

    public Coordinates(double x, double y) {
        this.xPos = x;
        this.yPos = y;
    }

    public void setCoordinates(double x, double y) {
        this.xPos = x;
        this.yPos = y;
    }

    public void addVectorToPos(double angle, double magnitude) {
        double deltaX = magnitude * Math.cos(Math.toRadians(angle));
        double deltaY = magnitude * Math.sin(Math.toRadians(angle));
        xPos += deltaX;
        yPos += deltaY;
    }

    public double getX() {
        return xPos;
    }

    public double getY() {
        return yPos;
    }

    public static double getAngle(Coordinates c1, Coordinates c2) {
        double direction = Math.toDegrees(Math.atan2(c2.getY() - c1.getY(), c2.getX() - c1.getX()));
        return direction;
    }

    public static double getMagnitude(Coordinates c1, Coordinates c2) {
        double magnitude = Math.sqrt(Math.pow(c2.getX() - c1.getX(), 2) +
                Math.pow(c2.getY() - c1.getY(), 2));
        return magnitude;
    }
}
