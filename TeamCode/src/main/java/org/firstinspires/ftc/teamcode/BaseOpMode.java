package org.firstinspires.ftc.teamcode;

import com.qualcomm.hardware.bosch.BNO055IMU;
import com.qualcomm.hardware.lynx.LynxModule;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;
import com.qualcomm.robotcore.util.ThreadPool;

import org.firstinspires.ftc.teamcode.hardware.Drive;
import org.firstinspires.ftc.teamcode.hardware.HardwareConstants;
import org.firstinspires.ftc.teamcode.hardware.Lift;
import org.firstinspires.ftc.teamcode.hardware.RevImu;
import org.firstinspires.ftc.teamcode.hardware.RevSensors;
import org.firstinspires.ftc.teamcode.hardware.TankDrive;
import org.firstinspires.ftc.teamcode.utils.BotcatsLog;
import org.firstinspires.ftc.teamcode.utils.Coordinates;
import org.firstinspires.ftc.teamcode.utils.Module;
import org.firstinspires.ftc.teamcode.utils.RobotTts;
import org.firstinspires.ftc.teamcode.vision.CubeSampler;

import java.util.ArrayList;

import static org.firstinspires.ftc.teamcode.hardware.HardwareConstants.SERVO_MARKER_UP;
import static org.firstinspires.ftc.teamcode.vision.CubeSampler.Speed.SLOW;

public abstract class BaseOpMode extends LinearOpMode {

    private static BaseOpMode instance = null;

    // Creates an accessible instance on start of an OpMode
    public BaseOpMode() {
        instance = this;
    }

    public static BaseOpMode getInstance() {
        return instance;
    }


    //----------------------------------------------------------------------------------------------
    // Runtime Variables
    //----------------------------------------------------------------------------------------------

    // Logging
    public boolean loggingEnabled = true;

    public BotcatsLog debugLog;
    public BotcatsLog positionLog;


    public ArrayList<BotcatsLog> loggers;
    public ArrayList<Module> modules;

    // Module enablers
    public boolean cvEnabled = true;
    public boolean driveEnabled = true;
    public boolean liftEnabled = true;
    public boolean imuEnabled = true;
    public boolean coordinatesEnabled = true;
    public boolean revSensorsEnabled = false;
    public boolean ttsEnabled = true;
    public boolean markerDropEnabled = true;

    // Drive / Motors
    public Drive drive;

    public DcMotor motorLeft;
    public DcMotor motorRight;

    private ArrayList<DcMotor> leftMotors;
    private ArrayList<DcMotor> rightMotors;

    public Lift lift;

    public volatile Thread liftThread;
    public volatile Thread markerDropThread;

    DcMotor motorLift;

    // Servos / Auxiliary

    Servo servoPhone;
    DcMotor motorMarker;

    // Sensors
    public RevImu imu;
    public BNO055IMU hubImu;

    public RevSensors revSensors;
    public LynxModule revHub;

    public CubeSampler cvCubeSampler;

    // Telemetry
    public ElapsedTime runtime;

    // Etc.
    public Coordinates robotCoords;
    public RobotTts tts;

    //----------------------------------------------------------------------------------------------
    // Common Functions
    //----------------------------------------------------------------------------------------------

    void initialize() throws InterruptedException{

        modules = new ArrayList<>();

        if (loggingEnabled) {
            loggers = new ArrayList<>();
            loggers.add(debugLog);
        } else {
            debugLog = new BotcatsLog();
        }

        if (coordinatesEnabled) {
            robotCoords = new Coordinates(0, 0);
        }

        if (imuEnabled) {
            String imuName = "imu";
            hubImu = hardwareMap.get(BNO055IMU.class, imuName);
            imu = new RevImu(hubImu);
        } else {
            imu = new RevImu();
            if (loggingEnabled) {
                debugLog.addDbgMessage(
                        BotcatsLog.DbgLevel.INFO, "IMU", "Disabled");
            }
        }

        if (revSensorsEnabled) {
            String hubName = "Expansion Hub 1";
            revHub = hardwareMap.get(LynxModule.class, hubName);
            revSensors = new RevSensors(revHub);
        } else {
            revSensors = new RevSensors();
            if (loggingEnabled) {
                debugLog.addDbgMessage(
                        BotcatsLog.DbgLevel.INFO, "Rev Sensors", "Disabled");
            }
        }

        if (cvEnabled) {
            cvCubeSampler = new CubeSampler(SLOW);
            modules.add(cvCubeSampler);
            cvCubeSampler.startOpenCV();
        } else {
            cvCubeSampler = new CubeSampler();
            if (loggingEnabled) {
                debugLog.addDbgMessage(BotcatsLog.DbgLevel.INFO, "OpenCV", "Disabled");
            }
        }

        if (driveEnabled) {
            motorLeft = hardwareMap.dcMotor.get("motorLeft");
            motorRight = hardwareMap.dcMotor.get("motorRight");
            leftMotors = new ArrayList<>();
            rightMotors = new ArrayList<>();
            leftMotors.add(motorLeft);
            rightMotors.add(motorRight);

            drive = new TankDrive(leftMotors, rightMotors);
            drive.setPidCoefficients(Drive.loadPidCoefficients());
            drive.setMotorTicksPerRev();
            drive.setWheels(HardwareConstants.CIRC_2018_WCD);
        } else {
            drive = new Drive();
            if (loggingEnabled) {
                debugLog.addDbgMessage(
                        BotcatsLog.DbgLevel.INFO, "Drive", "Disabled");
            }
        }

        if (liftEnabled) {
            motorLift = hardwareMap.dcMotor.get("motorLift");
            lift = new Lift(motorLift);
        } else {
            lift = new Lift();
            if (loggingEnabled) {
                debugLog.addDbgMessage(
                        BotcatsLog.DbgLevel.INFO, "Lift", "Disabled");
            }
        }

        if (ttsEnabled) {
            tts = new RobotTts(hardwareMap.appContext);
        } else {
            tts = new RobotTts();
            if (loggingEnabled) {
                debugLog.addDbgMessage(
                        BotcatsLog.DbgLevel.INFO, "TTS", "Disabled");
            }
        }

        if (markerDropEnabled) {
            motorMarker = hardwareMap.dcMotor.get("motorMarker");
        }

        servoPhone = hardwareMap.servo.get("servoPhone");
        if (servoPhone != null) {
            servoPhone.setPosition(0.76);
        }

        switch (type) {
            case TELEOP:
                // Put any TeleOp-only hardware requirements here;
            case AUTONOMOUS:
                if (motorMarker != null) motorMarker.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
                loggers.add(positionLog);
        }
    }

    void shutdown() {
        if (driveEnabled) {
            drive.gentleStopAll();
        }

        if (ttsEnabled) {
            tts.stopTalking();
        }

        if (cvEnabled) {
            cvCubeSampler.stopOpenCV();
        }

        if (loggingEnabled && loggers.size() > 0) {
            debugLog.addDbgMessage(
                    BotcatsLog.DbgLevel.INFO,
                    "OpMode",
                    "Shutting Down"
            );
            for (BotcatsLog logger : loggers) {
                logger.closeLog();
            }
        }
        telemetry.addData("Detector: ", cvCubeSampler);
        telemetry.update();
    }

    public OpModeType getType() {
        return type;
    }

    public enum OpModeType {
        AUTONOMOUS,
        TELEOP,
        TEST
    }

    OpModeType type;
}
