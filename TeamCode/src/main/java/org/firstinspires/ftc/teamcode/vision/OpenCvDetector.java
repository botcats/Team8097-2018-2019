package org.firstinspires.ftc.teamcode.vision;

import org.firstinspires.ftc.robotcontroller.internal.FtcRobotControllerActivity;
import org.firstinspires.ftc.teamcode.utils.Module;
import org.firstinspires.ftc.teamcode.utils.BotcatsLog;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.core.Mat;

public abstract class OpenCvDetector extends Module implements CameraBridgeViewBase.CvCameraViewListener2 {

    protected Mat mRgba;

    OpenCvDetector() {
        super();
    }

    public void startOpenCV() {
        if (op.cvEnabled) {
            if (moduleLoggingEnabled) {
                this.op.debugLog.addDbgMessage(
                        BotcatsLog.DbgLevel.INFO,
                        "OpenCv",
                        "Camera starting"
                );
            }
            FtcRobotControllerActivity.mOpenCvCameraView.setMaxFrameSize(864, 480);
            FtcRobotControllerActivity.turnOnCameraView.obtainMessage(1, this).sendToTarget();
        }
    }

    public void stopOpenCV() {
        if (op.cvEnabled) {
            if (moduleLoggingEnabled) {
                this.op.debugLog.addDbgMessage(
                        BotcatsLog.DbgLevel.INFO,
                        "OpenCv",
                        "Camera stopping"
                );
            }
            FtcRobotControllerActivity.turnOffCameraView.obtainMessage().sendToTarget();
        }
    }

    @Override
    public abstract void onCameraViewStarted(int width, int height);

    @Override
    public abstract void onCameraViewStopped();

    @Override
    public abstract Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame);
}
