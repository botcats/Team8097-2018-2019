package org.firstinspires.ftc.teamcode;

import static org.firstinspires.ftc.teamcode.Autonomous.Team.RED;
import static org.firstinspires.ftc.teamcode.Autonomous.StartPosition.CRATER;

@com.qualcomm.robotcore.eventloop.opmode.Autonomous(name="Red Auto (Crater)", group ="Autonomous")
public class RedAutonomousCrater extends Autonomous {

    @Override
    void setAutoVars() {
        team = RED;
        startPosition = CRATER;
        angleOffset = 315;
        xOffset = xyOffset;
        yOffset = -xyOffset;
    }
}
