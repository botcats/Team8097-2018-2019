package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.util.ElapsedTime;
import org.firstinspires.ftc.teamcode.utils.BotcatsLog;
import static org.firstinspires.ftc.teamcode.BaseOpMode.OpModeType.TEST;

@com.qualcomm.robotcore.eventloop.opmode.TeleOp(name="Test TeleOp", group="Test")


public class TestTeleOp extends BaseOpMode {

    private ElapsedTime cooldown;
    private double buttonACooldown, buttonA2Cooldown, buttonYCooldown, buttonXCooldown,
            buttonBCooldown, buttonLBCooldown, buttonRBCooldown, buttonDLeftCooldown,
            buttonDRightCooldown, buttonDUpCooldown, buttonDDownCooldown;

    public void runOpMode() throws InterruptedException {

        type = TEST;

        runtime = new ElapsedTime();
        cooldown = new ElapsedTime();

        debugLog = new BotcatsLog(runtime, "Test Op", BotcatsLog.LogType.DEBUG);
        debugLog.openLog();

        initialize();

        telemetry.addData("Modules Loaded: ", modules);
        telemetry.addData("Loggers Running: ", loggers);
        telemetry.update();

        waitForStart();

        runtime.reset();

        while (opModeIsActive()) {



            telemetry.addData("Lift position", motorLift.getCurrentPosition());
            telemetry.update();
            if (isStopRequested()) {
                shutdown();
            }
        }
    }
}
