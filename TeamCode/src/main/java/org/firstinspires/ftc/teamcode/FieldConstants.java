package org.firstinspires.ftc.teamcode;

import org.firstinspires.ftc.teamcode.utils.Coordinates;

public final class FieldConstants {
    // A class mostly for coordinates of things on the field and whatever else we need that remains constant.
    public static final Coordinates ORIGIN = new Coordinates(0, 0);

    public static final Coordinates DEPOT_BLUE = new Coordinates(-58, -58);
    public static final Coordinates DEPOT_BLUE_TOP = new Coordinates(-58, -40);
    public static final Coordinates DEPOT_BLUE_RIGHT = new Coordinates(-40, -58);

    public static final Coordinates DEPOT_RED = new Coordinates(58, 58);
    public static final Coordinates DEPOT_RED_BOTTOM = new Coordinates(58, 40);
    public static final Coordinates DEPOT_RED_LEFT = new Coordinates(40, 60);

    public static final Coordinates CRATER_BLUE = new Coordinates(-58, 58);
    public static final Coordinates CRATER_BLUE_BOTTOM = new Coordinates(-58, 30);
    public static final Coordinates CRATER_BLUE_RIGHT = new Coordinates(-30, 58);


    public static final Coordinates CRATER_RED = new Coordinates(58, -58);
    public static final Coordinates CRATER_RED_TOP = new Coordinates(58, -30);
    public static final Coordinates CRATER_RED_LEFT = new Coordinates(30, -58);

    public static final Coordinates SAMPLE_DEPOT_BLUE = new Coordinates(-23.45, -23.45);
    public static final Coordinates SAMPLE_CRATER_BLUE = new Coordinates(-23.45, 23.45);
    public static final Coordinates SAMPLE_DEPOT_RED = new Coordinates(23.45, 23.45);
    public static final Coordinates SAMPLE_CRATER_RED = new Coordinates(23.45, -23.45);
    public static final Coordinates MIDPOINT_BLUE = new Coordinates(-58, 0);
    public static final Coordinates MIDPOINT_RED = new Coordinates(58, 0);

}
