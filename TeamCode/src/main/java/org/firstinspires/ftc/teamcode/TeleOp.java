package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.teamcode.utils.BotcatsLog;
import org.firstinspires.ftc.teamcode.utils.RobotTts;

import static org.firstinspires.ftc.teamcode.BaseOpMode.OpModeType.TELEOP;

@com.qualcomm.robotcore.eventloop.opmode.TeleOp(name="TeleOp", group="TeleOp")
public class TeleOp extends BaseOpMode {

    private ElapsedTime cooldown;
    private boolean slowMode = false;
    private boolean manualSet = false;
    private int langIndex = 0;
    private double buttonACooldown, buttonA2Cooldown, buttonYCooldown, buttonXCooldown, buttonBCooldown, buttonLBCooldown, buttonRBCooldown;

    @Override
    public void runOpMode() throws InterruptedException{

        type = TELEOP;

        telemetry.addLine("INITIALIZING. DO NOT PRESS START");
        telemetry.update();

        runtime = new ElapsedTime();
        cooldown = new ElapsedTime();

        debugLog = new BotcatsLog(runtime, "TeleOp", BotcatsLog.LogType.DEBUG);
        debugLog.openLog();

        initialize();

        telemetry.addData("Status", "Initialized");
        telemetry.update();

        liftThread = new Thread(() -> {
            while (!Thread.currentThread().isInterrupted() && opModeIsActive()) {
                if (gamepad1.dpad_up) {
                    motorLift.setPower(-1);
                } else if (gamepad1.dpad_down) {
                    motorLift.setPower(1);
                } else {
                    motorLift.setPower(0);
                }
                if (gamepad1.y && Math.abs(cooldown.time() - buttonYCooldown) >= .2) {
                    lift.moveTo(lift.EXTENDED_TICKS);
                    buttonYCooldown = cooldown.time();
                }
                if (gamepad1.a && Math.abs(cooldown.time() - buttonACooldown) >= .2) {
                    lift.moveTo(lift.RETRACTED_TICKS);
                    buttonACooldown = cooldown.time();
                }
                if (gamepad1.b && Math.abs(cooldown.time() - buttonBCooldown) >= .2) {
                    lift.moveTo(lift.CATCH_RESET_TICKS);
                    buttonBCooldown = cooldown.time();
                }
            }
        });

        tts.lang = tts.randomLang();

        boolean lastInput = false, lastLeft = false, lastRight = false;

        //------------------------------------------------------------------------------------------
        // Pre-run Menu
        //------------------------------------------------------------------------------------------

        while (!opModeIsActive()) {
            telemetry.addLine("Use Dpad to select Language");

            if ((gamepad1.dpad_right || gamepad1.dpad_right) && !lastRight) {
                manualSet = true;
                langIndex++;
                langIndex = (langIndex > RobotTts.Language.values().length - 1) ? 0 : langIndex;
                telemetry.update();
            }
            else if ((gamepad1.dpad_left || gamepad2.dpad_left) && !lastLeft) {
                manualSet = true;
                langIndex--;
                langIndex = (langIndex < 0) ? RobotTts.Language.values().length - 1 : langIndex;
                telemetry.update();
            }

            if (manualSet) {
                tts.lang = RobotTts.Language.values()[langIndex];
            }

            telemetry.addData("Language: ", tts.lang);
            telemetry.addData("Modules Loaded: ", modules);
            telemetry.addData("Loggers Running: ", loggers);
            telemetry.update();

            lastLeft = gamepad1.dpad_left || gamepad2.dpad_left;
            lastRight = gamepad1.dpad_right || gamepad2.dpad_right;

            if (isStopRequested()) {
                shutdown();
                break;
            }
        }

        waitForStart();

        if (loggingEnabled) {
            debugLog.addDbgMessage(
                    BotcatsLog.DbgLevel.INFO,
                    "OpMode",
                    "---------- Starting TeleOp ----------"
            );
            if (slowMode) {
                debugLog.addDbgMessage(
                        BotcatsLog.DbgLevel.INFO,
                        "OpMode",
                        "OUTREACH MODE ON"
                );
            }
        }

        runtime.reset();

        tts.setLanguage();
        tts.speak(tts.welcomeText());

        liftThread.start();

        while (opModeIsActive()) {

            // Telemetry
            buildTelemetry();

            drive.curveDrive(gamepad1.left_stick_y, gamepad1.left_trigger, gamepad1.right_trigger);
            if (slowMode) {
                drive.curveDrive(gamepad1.left_stick_y / 3, gamepad1.left_trigger / 2, gamepad1.right_trigger / 2);
            } else {
                drive.curveDrive(gamepad1.left_stick_y, gamepad1.left_trigger, gamepad1.right_trigger);
            }

            if (gamepad2.a && Math.abs(cooldown.time() - buttonA2Cooldown) >= .2) {
                tts.speak(tts.getRandomLine());
            }


            if (gamepad1.x && Math.abs(cooldown.time() - buttonXCooldown) >= .2) {
                slowMode = !slowMode;
                buttonXCooldown = cooldown.time();
            }

            if (isStopRequested()) {
                if (loggingEnabled) {
                    debugLog.addDbgMessage(
                            BotcatsLog.DbgLevel.INFO,
                            "OpMode",
                            "---------- Stopping TeleOp ----------"
                    );
                }
                shutdown();
            }
        }
    }

    private void buildTelemetry() {
        if (slowMode) telemetry.addLine("Slow Mode Enabled");
        telemetry.addData("Language: ", tts.lang);
        telemetry.addData("Heading: ", imu.getHeading());
        telemetry.addData("Drive enabled: ", driveEnabled);
        telemetry.addData("Position enabled: ", imuEnabled);
        telemetry.addData("Left", motorLeft.getCurrentPosition());
        telemetry.addData("Right", motorRight.getCurrentPosition());

        telemetry.update();
    }
}
