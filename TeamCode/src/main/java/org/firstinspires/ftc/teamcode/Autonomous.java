package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.teamcode.utils.BotcatsLog;
import org.firstinspires.ftc.teamcode.utils.Coordinates;

import java.lang.reflect.Field;

import static org.firstinspires.ftc.teamcode.Autonomous.AutoPhase.CLAIMING;
import static org.firstinspires.ftc.teamcode.Autonomous.AutoPhase.INIT;
import static org.firstinspires.ftc.teamcode.Autonomous.AutoPhase.LANDING;
import static org.firstinspires.ftc.teamcode.Autonomous.AutoPhase.MOVE_CRATER;
import static org.firstinspires.ftc.teamcode.Autonomous.AutoPhase.MOVE_DEPOT;
import static org.firstinspires.ftc.teamcode.Autonomous.AutoPhase.MOVE_SAMPLE;
import static org.firstinspires.ftc.teamcode.Autonomous.AutoPhase.SAMPLING;
import static org.firstinspires.ftc.teamcode.Autonomous.CubePosition.CENTER;
import static org.firstinspires.ftc.teamcode.Autonomous.CubePosition.LEFT;
import static org.firstinspires.ftc.teamcode.Autonomous.CubePosition.NONE;
import static org.firstinspires.ftc.teamcode.Autonomous.CubePosition.RIGHT;
import static org.firstinspires.ftc.teamcode.Autonomous.StartPosition.CRATER;
import static org.firstinspires.ftc.teamcode.Autonomous.StartPosition.DEPOT;
import static org.firstinspires.ftc.teamcode.Autonomous.Team.BLUE;
import static org.firstinspires.ftc.teamcode.Autonomous.Team.RED;
import static org.firstinspires.ftc.teamcode.BaseOpMode.OpModeType.AUTONOMOUS;
import static org.firstinspires.ftc.teamcode.hardware.HardwareConstants.SERVO_MARKER_DOWN;
import static org.firstinspires.ftc.teamcode.hardware.HardwareConstants.SERVO_MARKER_UP;

public abstract class Autonomous extends BaseOpMode {

    float angleOffset = 0;
    private float robotOffset = 8.8f;
    private final float LANDER_OFFSET = 13.3f;
    
    private final float MOVE_SPEED = 0.75f;
    private final float TURN_SPEED = 0.75f;
    
    private float totalOffset = robotOffset + LANDER_OFFSET;
    double xyOffset = Math.sqrt(Math.pow(totalOffset, 2) / 2);
    double xOffset;
    double yOffset;

    public volatile Thread markerDropThread;
    public volatile Thread liftThread;

    @Override
    public void runOpMode() throws InterruptedException {

        type = AUTONOMOUS;

        runtime = new ElapsedTime();

        setAutoVars();

        debugLog = new BotcatsLog(runtime, "Autonomous", BotcatsLog.LogType.DEBUG);
        positionLog = new BotcatsLog(runtime, "Position", BotcatsLog.LogType.DATA);
        debugLog.openLog();
        positionLog.openLog();

        initialize();

        telemetry.addData("Status", "Initialized");
        telemetry.addData("Team: ", team);
        telemetry.addData("Position: ", startPosition);
        telemetry.addData("Heading: ", imu.getHeading());
        telemetry.addData("IMU: ", imu);
        telemetry.update();
        tts.lang = tts.randomLang();

        phase = INIT;

        robotCoords.setCoordinates(xOffset, yOffset);
        imu.setHeadingOffset(angleOffset);

        liftThread = new Thread(() -> {
            while (!Thread.currentThread().isInterrupted() && opModeIsActive()) {
                lift.retract();
                liftThread = null;
            }
        });

        markerDropThread = new Thread(() -> {
            while (markerDropThread == Thread.currentThread()) {
                sleep(1000);
                motorMarker.setTargetPosition(-(int) motorMarker.getMotorType().getTicksPerRev() / 2);
                motorMarker.setMode(DcMotor.RunMode.RUN_TO_POSITION);
                motorMarker.setPower(.75);
                while (motorMarker.isBusy()) { }
                motorMarker.setPower(0);
                motorMarker.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                sleep(100);
                markerDropThread = null;
            }
        });

        if (loggingEnabled) {
            debugLog.addDbgMessage(
                    BotcatsLog.DbgLevel.INFO,
                    "OpMode",
                    "X: " + robotCoords.getX() + " Y: " + robotCoords.getY()
            );
        }

        while(!opModeIsActive()) {
            if (servoPhone != null) {
                telemetry.addData("Position: ", servoPhone.getPosition());
                telemetry.update();
                if (gamepad1.dpad_up) {
                    servoPhone.setPosition(servoPhone.getPosition() - 0.001);
                } else if (gamepad1.dpad_down) {
                    servoPhone.setPosition(servoPhone.getPosition() + 0.001);
                }
            }

            if (isStopRequested()) {
                shutdown();
                break;
            }
        }

        waitForStart();

        tts.setLanguage();
        tts.speak(tts.welcomeText());

        if (loggingEnabled) {
            debugLog.addDbgMessage(
                    BotcatsLog.DbgLevel.INFO,
                    "OpMode",
                    "---------- Starting Autonomous ----------"
            );
            debugLog.addDbgMessage(
                    BotcatsLog.DbgLevel.INFO,
                    "OpMode",
                    "TEAM: " + team
            );
            debugLog.addDbgMessage(
                    BotcatsLog.DbgLevel.INFO,
                    "OpMode",
                    "POSITION: " + startPosition
            );
        }

        runtime.reset();
        detectCube();
        lower();
        liftThread.start();
        knockCube();
        moveSample();
        if (startPosition == DEPOT) {
            goToDepot();
            goToCrater();
        }
        shutdown();
        requestOpModeStop();

    }

    // Neat auto functions to go in that while loop right over there
    void lower() throws InterruptedException {
        if (opModeIsActive()) {
            phase = LANDING;
            telemetry.addLine("Landing");
            telemetry.update();
            if (loggingEnabled) {
                debugLog.addDbgMessage(
                        BotcatsLog.DbgLevel.INFO,
                        "OpMode",
                        "Landing"
                );
            }
            lift.extend();
        }
    }

    void detectCube() {
        if (opModeIsActive()) {
            telemetry.addLine("Detecting Cube");
            telemetry.update();
            phase = SAMPLING;
            double detectStartTime = runtime.time();

            if (loggingEnabled) {
                debugLog.addDbgMessage(
                        BotcatsLog.DbgLevel.INFO,
                        "OpMode",
                        "Detect Start Time: " + detectStartTime
                );
            }
            if (cvEnabled) {
                do {
                    cubePosition = cvCubeSampler.getCubePosition();
                    if (runtime.time() - detectStartTime >= 10) {
                        debugLog.addDbgMessage(
                                BotcatsLog.DbgLevel.INFO,
                                "OpMode",
                                "Detect Start Time: " + detectStartTime
                        );
                        debugLog.addDbgMessage(
                                BotcatsLog.DbgLevel.INFO,
                                "OpMode",
                                "Runtime: " + runtime.time()
                        );
                        if (loggingEnabled) {
                            debugLog.addDbgMessage(
                                    BotcatsLog.DbgLevel.WARN,
                                    "OpMode",
                                    "Cube not detected in time! Defaulting to center"
                            );
                        }
                        cubePosition = CENTER;
                        break;
                    }
                } while (cubePosition == NONE && opModeIsActive());
            } else {
                cubePosition = CENTER;
            }
            if (loggingEnabled) {
                debugLog.addDbgMessage(
                        BotcatsLog.DbgLevel.INFO,
                        "OpMode",
                        "Found cube " + cubePosition
                );
            }
            if (ttsEnabled) {
                tts.speak("Found the " + cubePosition.toString() + " cube");
            }
        }
    }

    void moveSample() throws  InterruptedException {
        if (opModeIsActive()) {
            phase = MOVE_SAMPLE;
            if (team == RED) {
                if (startPosition == DEPOT) {
                    drive.moveToCoordinates(FieldConstants.SAMPLE_DEPOT_RED, MOVE_SPEED);
                } else if (startPosition == CRATER) {
                    drive.moveToCoordinates(FieldConstants.SAMPLE_CRATER_RED, MOVE_SPEED);
                }
            } else if (team == BLUE) {
                if (startPosition == DEPOT) {
                    drive.moveToCoordinates(FieldConstants.SAMPLE_DEPOT_BLUE, MOVE_SPEED);
                } else if (startPosition == CRATER) {
                    drive.moveToCoordinates(FieldConstants.SAMPLE_CRATER_BLUE, MOVE_SPEED);
                }
            }
        }
    }

    void knockCube() throws InterruptedException {
        if (opModeIsActive()) {
            telemetry.addLine("Knocking Cube");
            telemetry.addData("Position: ", cubePosition);
            telemetry.update();
            if (loggingEnabled) {
                debugLog.addDbgMessage(
                        BotcatsLog.DbgLevel.INFO,
                        "OpMode",
                        "Moving to cube " + cubePosition
                );
            }
            switch (cubePosition) {
                case RIGHT:
                    drive.turnRightFromCurrent(45, TURN_SPEED);
                    drive.goDistance(22.5, MOVE_SPEED / 4);
                    if (startPosition == CRATER) {
//                        drive.goDistance(-22.5, MOVE_SPEED);
                    }
                    break;
                case CENTER:
                    drive.goDistance(15.5, MOVE_SPEED / 4);
                    if (startPosition == CRATER) {
//                        drive.goDistance(-15.5, MOVE_SPEED);
                    }
                    break;
                case LEFT:
                    drive.turnLeftFromCurrent(45, TURN_SPEED);
                    drive.goDistance(22.5, MOVE_SPEED / 4);
                    break;
            }
            if (startPosition == DEPOT && cubePosition != CENTER) {
                drive.goDistance(4, MOVE_SPEED / 2);
            }
            if (startPosition == CRATER) {
                if (team == RED) {
                    drive.moveToCoordinates(FieldConstants.CRATER_RED, MOVE_SPEED);
                } else {
                    drive.moveToCoordinates(FieldConstants.CRATER_BLUE, MOVE_SPEED);

                }
            }
        }
    }

    void goToDepot() throws InterruptedException {
        if (opModeIsActive()) {
            telemetry.addLine("Going to Depot");
            telemetry.update();
            phase = MOVE_DEPOT;
            if (team == RED) {
                if (startPosition == CRATER) {
                    drive.moveToCoordinates(FieldConstants.MIDPOINT_RED, MOVE_SPEED);
                    drive.moveToCoordinates(FieldConstants.DEPOT_RED_BOTTOM, MOVE_SPEED);
                } else if (startPosition == DEPOT) {
                    drive.moveToCoordinates(FieldConstants.DEPOT_RED, MOVE_SPEED);
                }
                drive.moveToCoordinates(FieldConstants.DEPOT_RED, MOVE_SPEED);
            } else if (team == BLUE) {
                if (startPosition == CRATER) {
                    drive.moveToCoordinates(FieldConstants.MIDPOINT_BLUE, MOVE_SPEED);
                    drive.moveToCoordinates(FieldConstants.DEPOT_BLUE_TOP, MOVE_SPEED);
                } else if (startPosition == DEPOT) {
                    drive.moveToCoordinates(FieldConstants.DEPOT_BLUE, MOVE_SPEED);
                }

            }
        }
    }


    void goToCrater() throws InterruptedException {
        phase = MOVE_CRATER;
        drive.goDistance(3, 1);
        markerDropThread.start();
        if (team == BLUE) {
            if (startPosition == DEPOT){
                drive.moveToCoordinates(FieldConstants.CRATER_RED_LEFT, MOVE_SPEED);
            } else if (startPosition == CRATER){
                drive.moveToCoordinates(FieldConstants.CRATER_BLUE_BOTTOM, MOVE_SPEED);
            }
        } else if (team == RED){
            if (startPosition == DEPOT){
                drive.moveToCoordinates(FieldConstants.CRATER_BLUE_RIGHT, MOVE_SPEED);
            } else if (startPosition == CRATER) {
                drive.moveToCoordinates(FieldConstants.CRATER_RED_TOP, MOVE_SPEED);
            }
        }
    }

    abstract void setAutoVars();

    enum Team {
        RED,
        BLUE
    }

    enum StartPosition {
        CRATER,
        DEPOT
    }

    public enum CubePosition {
        LEFT,
        CENTER,
        RIGHT,
        NONE
    }

    enum AutoPhase {
        INIT,
        LANDING,
        MOVE_SAMPLE,
        SAMPLING,
        MOVE_DEPOT,
        CLAIMING,
        MOVE_CRATER,
    }

    Team team;
    StartPosition startPosition;
    CubePosition cubePosition;
    AutoPhase phase;
}
