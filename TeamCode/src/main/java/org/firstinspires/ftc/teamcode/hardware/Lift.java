package org.firstinspires.ftc.teamcode.hardware;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;

import org.firstinspires.ftc.teamcode.BaseOpMode;
import org.firstinspires.ftc.teamcode.utils.BotcatsLog;
import org.firstinspires.ftc.teamcode.utils.Module;

import static java.lang.Thread.sleep;

public class Lift extends Module {

    private DcMotor motorActuator;

    final private double PRIMARY_SPEED = 1;

    final public int RETRACTED_TICKS = 0;
    final public int CATCH_RESET_TICKS = 17000;
    final public int EXTENDED_TICKS = 22000;

    public Lift(DcMotor motorLift) {
        super();

        moduleName = "Lift";
        this.motorActuator = motorLift;
        if (motorActuator != null) {
            motorActuator.setDirection(DcMotorSimple.Direction.REVERSE);
            motorActuator.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
            motorActuator.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
            motorActuator.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

            if (moduleLoggingEnabled) {
                op.debugLog.addDbgMessage(
                        BotcatsLog.DbgLevel.INFO,
                        moduleName,
                        "Initialized"
                );
            }

            op.modules.add(this);

        } else {
            if (moduleLoggingEnabled) {
                op.debugLog.addDbgMessage(
                        BotcatsLog.DbgLevel.INFO,
                        moduleName,
                        "Not Found"
                );
            }
            op.liftEnabled = false;
        }
    }

    public Lift() { }

    public void extend() {
        if (op.liftEnabled) {
            moveTo(EXTENDED_TICKS);
        }
    }

    public void retract() {
        if (op.liftEnabled) {
            moveTo(RETRACTED_TICKS);
        }
    }

    public void moveTo(int ticks) {
        if (op.liftEnabled) {
            if (moduleLoggingEnabled) {
                op.debugLog.addDbgMessage(
                        BotcatsLog.DbgLevel.INFO,
                        moduleName,
                        "Lift moving to " + ticks
                );
            }

            motorActuator.setTargetPosition(ticks);
            motorActuator.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            motorActuator.setPower(PRIMARY_SPEED);

            while (motorActuator.isBusy()  && op.opModeIsActive()) { }

            motorActuator.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
            motorActuator.setPower(0);
        }
    }
}
