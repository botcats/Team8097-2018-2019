package org.firstinspires.ftc.teamcode;

import static org.firstinspires.ftc.teamcode.Autonomous.Team.BLUE;
import static org.firstinspires.ftc.teamcode.Autonomous.StartPosition.CRATER;

@com.qualcomm.robotcore.eventloop.opmode.Autonomous(name="Blue Auto (Crater)", group ="Autonomous")
public class BlueAutonomousCrater extends Autonomous {

    @Override
    void setAutoVars() {
        team = BLUE;
        startPosition = CRATER;
        angleOffset = 135;
        xOffset = -xyOffset;
        yOffset = xyOffset;
    }
}
