package org.firstinspires.ftc.teamcode.utils;

import android.content.Context;
import android.speech.tts.TextToSpeech;

import org.firstinspires.ftc.teamcode.BaseOpMode;

import java.util.Locale;
import java.util.Random;

import static org.firstinspires.ftc.teamcode.utils.RobotTts.Language.CHINESE;
import static org.firstinspires.ftc.teamcode.utils.RobotTts.Language.ENGLISH;
import static org.firstinspires.ftc.teamcode.utils.RobotTts.Language.GERMAN;
import static org.firstinspires.ftc.teamcode.utils.RobotTts.Language.JAPANESE;
import static org.firstinspires.ftc.teamcode.utils.RobotTts.Language.KOREAN;
import static org.firstinspires.ftc.teamcode.utils.RobotTts.Language.RUSSIAN;

public class RobotTts extends Module {

    private TextToSpeech tts;
    
    private Random generator = new Random();

    public RobotTts(Context context) {
        super();
        tts = new TextToSpeech(context, null);
        tts.setPitch(1.5f);
        tts.setSpeechRate(1.5f);
        if (moduleLoggingEnabled) {
            op.debugLog.addDbgMessage(
                    BotcatsLog.DbgLevel.INFO,
                    "TTS",
                    "Initialized"
            );
        }
        op.modules.add(this);
    }

    public RobotTts() { }

    public void speak(String text) {
        if (op.ttsEnabled) {
            tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
            if (moduleLoggingEnabled) {
                op.debugLog.addDbgMessage(
                        BotcatsLog.DbgLevel.INFO,
                        "TTS",
                        text
                );
            }
        }
    }

    public void setLanguage() {
        if (op.ttsEnabled) {
            tts.setLanguage(lang.langLocale);

            if (moduleLoggingEnabled) {
                op.debugLog.addDbgMessage(
                        BotcatsLog.DbgLevel.INFO,
                        "TTS",
                        "Language set to " + lang
                );
            }
        }
    }

    public void setLanguage(Language newLanguage) {
        if (op.ttsEnabled) {
            lang = newLanguage;
            tts.setLanguage(lang.langLocale);
            if (moduleLoggingEnabled) {
                op.debugLog.addDbgMessage(
                        BotcatsLog.DbgLevel.INFO,
                        "TTS",
                        "Language set to " + lang
                );
            }
        }
    }

    public void stopTalking() {
        if (op.ttsEnabled) {
            tts.stop();
        }
    }

    String[] randomLines(){
        switch (lang) {
            case JAPANESE:
                return new String[] {
                        "鳩が飛んだわ,　鞄を持って, 昨日のあなたに, レッツゴー, レッツゴー",
                        "僕は音楽家 電卓片手に",
                        "いますぐ　やめろ",
                        "あんたバカァ?!",
                        "Niko niko nii",
                        "Kansei dorifto",
                        "Install Gentoo"
                };
            case KOREAN:
                return new String[] {
                        "감사합니다",
                        "대박. 우리는 해냈다.",
                        "오빠, 내 마음이 펄럭 거든."
                };
            case CHINESE:
                /* Thanks for these lines you sneaky sods*/
                return new String[] {
                        "我想我们已经输了",
                        "妈妈你看接了吗",
                        "我们想修这个机器人",
                        "机器猫，准备摧毁羊",
                        "四是四。十是十。十四是四十。四十是四十。四十四是四十四",
                        "吃 葡 萄 不 吐 葡 萄 皮 ,不 吃 葡 萄 倒 吐 葡 萄 皮"
                };
            case ENGLISH:
                return new String[] {
                        "Speed and power",
                        "Green is my pepper",
                        "Install Gentoo",
                        "Be your best",
                        "I have low self esteem",
                        "Moving up in the world doesn't mean using the lift, mate",
                        "To many men life is a failure; a poison-worm gnaws at their heart. " +
                                "Then at least let their dying be a success.",
                        "I have a big personality",
                        "I'm the operator with my pocket calculator",
                };
            case GERMAN:
                return new String[] {
                        "Geschwindigkeit und Kraft",
                        "Sei dein Bestes",
                        "Grün ist mein Pfeffer",
                        "Vors uns liegt ein weites tal die sonne scheint mit glitzerstrahl, " +
                                "die fahrbahn ist ein graues band.",
                        "Empfangen und genähret, vom Weibe wunderbar, kömmt er und sieht und höret, " +
                                "und nimmt des Trugs nicht wahr; gelüstet und begehret",
                        "Ich bin der musikant mit taschenrechner in der hand",
                        "Reichskänguruh Selbstfahrlafette mit Flugabwehrkanone 36 auf " +
                                "Panzerkampfwagen III und Sonderkraftfahrzeug 251, Ausführung K",
                        "Warenfetischismus"

                };
            case RUSSIAN:
                return new String[] {
                        "Мороз и солнце, день чудесный! Еще ты дремлещь, друг прелестный",
                        "Я сказал",
                        "Элементарно, дорогой друг",
                        "Что такое осень, это небо",
                        "Вспоминаю собаку, она, как звезда, ну и пусть"
                };
            default:
                return null;
        }
    }

    public String getRandomLine() {
        int i = generator.nextInt(randomLines().length);
        return randomLines()[i];
    }

    public Language randomLang() {
        int i = generator.nextInt(Language.values().length);
        return Language.values()[i];
    }

    public String welcomeText(){
        switch (lang) {
            case JAPANESE:
                return "Kawaii neko robotto-chan is ready, senpai";
            case KOREAN:
                return "안녕하세요";
            case CHINESE:
                return "大家好。我们开始";
            case ENGLISH:
                return "Prepare for disappointment";
            case GERMAN:
                return "Willkommen in unserem Panzerkampfwagen";
            case RUSSIAN:
                return "Пожеляй мне удачи в бою";
            default:
                return null;
        }
    }

    public enum Language {
        ENGLISH (Locale.UK),
        JAPANESE (Locale.JAPAN),
        KOREAN (Locale.KOREA),
        CHINESE (Locale.CHINA),
        GERMAN (Locale.GERMANY),
        RUSSIAN (new Locale("ru", "RU"));

        Locale langLocale;
        Language(Locale sysLocale) {
            this.langLocale = sysLocale;
        }
    }

    public Language lang;
}
