package org.firstinspires.ftc.teamcode.hardware;

public final class HardwareConstants {
    public static final int TICKS_PER_REV_NEVEREST_20 = 560;
    public static final int TICKS_PER_REV_NEVEREST_20_ORBITAL = 538;
    public static final int TICKS_PER_REV_NEVEREST_40 = 1120;
    public static final int TICKS_PER_REV_NEVEREST_60 = 1680;

    // All distances are in inches because FIRST as well as every hardware sourcing website are Americans
    // who like to use inches for everything

    public static final double CIRC_STEALTH_4 = 12.56;
    public static final double CIRC_MECANUM_STANDARD = 12.56;
    public static final double CIRC_2018_WCD = 13.35;

    public static final double CIRC_SPOOL_PARACORD = 3.71;

    public static final double SERVO_MARKER_UP = 0.75;
    public static final double SERVO_MARKER_DOWN = 0.20;

    public static final double KP_TURN = 0.0067;
    public static final double KI_TURN = 0;
    public static final double KD_TURN = 0.0011;

    public static final double KP_DRIVE_ANGLE = 0.002;
    public static final double KI_DRIVE_ANGLE = 0;
    public static final double KD_DRIVE_ANGLE = 0.0018;

    public static final double KP_DRIVE_SPEED = 0.0015;
    public static final double KI_DRIVE_SPEED = 0;
    public static final double KD_DRIVE_SPEED = 0.000175;

}
