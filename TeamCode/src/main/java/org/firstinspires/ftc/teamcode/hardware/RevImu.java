package org.firstinspires.ftc.teamcode.hardware;

import com.qualcomm.hardware.bosch.BNO055IMU;

import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import org.firstinspires.ftc.robotcore.external.navigation.AxesOrder;
import org.firstinspires.ftc.robotcore.external.navigation.AxesReference;
import org.firstinspires.ftc.robotcore.external.navigation.Orientation;
import org.firstinspires.ftc.teamcode.utils.BotcatsLog;
import org.firstinspires.ftc.teamcode.utils.Module;

import static java.lang.Thread.sleep;

public class RevImu extends Module {

    private BNO055IMU imu;
    private Orientation angles;
    private Orientation lastAngles = new Orientation();
    private float angle;

    public final int REV_HORIZ = 0;
    public final int REV_VERT = 1;
    final double COLLISION_DELTA = 4; // lel idk

    private double lastAccel;

    public RevImu(BNO055IMU hubImu) throws InterruptedException{
        super();
        BNO055IMU.Parameters parameters = new BNO055IMU.Parameters();
        parameters.angleUnit = BNO055IMU.AngleUnit.DEGREES;
        parameters.accelUnit = BNO055IMU.AccelUnit.METERS_PERSEC_PERSEC;
        parameters.calibrationDataFile = "BNO055IMUCalibration.json";

        moduleName = "IMU";
        this.imu = hubImu;
        if (imu != null) {
            imu.initialize(parameters);

            if (moduleLoggingEnabled) {
                op.debugLog.addDbgMessage(
                        BotcatsLog.DbgLevel.INFO,
                        moduleName,
                        "Initialized"
                );
            }

            op.modules.add(this);

        } else {
            if (moduleLoggingEnabled) {
                op.debugLog.addDbgMessage(
                        BotcatsLog.DbgLevel.INFO,
                        moduleName,
                        "Not Found"
                );
            }
            op.imuEnabled = false;
        }
    }

    public RevImu() { }

    public float getHeading() {
        if (op.imuEnabled) {
            angles = imu.getAngularOrientation(AxesReference.EXTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);

            // Second angle is roll, for when the hubs are mounted vertically and hardware wants to cause us extra pain like they do.
            float angleDiff = angles.firstAngle- lastAngles.firstAngle;

            if (angleDiff < -180) angleDiff += 360;
            else if (angleDiff > 180) angleDiff -= 360;

            angle += angleDiff;

            lastAngles = angles;

            return angle;
        } else {
            return 0;
        }
    }

    public float[] getAngles() {
        angles = imu.getAngularOrientation(AxesReference.EXTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);
        return new float[] {angles.firstAngle, angles.secondAngle, angles.thirdAngle};
    }

    public double getAccelMag() {
        return Math.sqrt(Math.pow(imu.getAcceleration().xAccel, 2) + Math.pow(imu.getAcceleration().yAccel, 2));
    }

    public boolean collisionFound() {
        double currentAccel = getAccelMag();
        double da = currentAccel - lastAccel;
        if (lastAccel != 0 && currentAccel - lastAccel > COLLISION_DELTA) {
            lastAccel = 0;
            return true;
        }
        else {
            lastAccel = currentAccel;
            return false;
        }
    }

    public void stopTracking() {
        if (op.imuEnabled) {
            imu.close();
        }
    }

    public void setOrientation(int orientation) throws InterruptedException{
        byte AXIS_MAP_CONFIG_BYTE = 0x6;
        byte AXIS_MAP_SIGN_BYTE;
        if (orientation == 1) {
            AXIS_MAP_SIGN_BYTE = 0x1;
        } else {
            AXIS_MAP_SIGN_BYTE = 0x0;
        }
        imu.write8(BNO055IMU.Register.OPR_MODE, BNO055IMU.SensorMode.CONFIG.bVal & 0x0F);
        sleep(100);
        imu.write8(BNO055IMU.Register.AXIS_MAP_CONFIG, AXIS_MAP_CONFIG_BYTE & 0x0F);
        imu.write8(BNO055IMU.Register.AXIS_MAP_SIGN, AXIS_MAP_SIGN_BYTE & 0x0F);
        imu.write8(BNO055IMU.Register.OPR_MODE, BNO055IMU.SensorMode.IMU.bVal & 0x0F);
        sleep(100);
    }

    public void setHeadingOffset(double initialAngle) {
        angle += initialAngle;
    }
}
