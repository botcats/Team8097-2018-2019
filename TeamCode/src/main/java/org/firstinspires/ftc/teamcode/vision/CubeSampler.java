package org.firstinspires.ftc.teamcode.vision;

import org.firstinspires.ftc.teamcode.Autonomous;
import org.firstinspires.ftc.teamcode.utils.BotcatsLog;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;

import static org.firstinspires.ftc.teamcode.vision.CubeSampler.Speed.SLOW;

public class CubeSampler extends OpenCvDetector {

    private Mat resizeOut = new Mat();
    private Mat blurOut = new Mat();

    private Mat colorCvtOut = new Mat();
    private Mat hsvThreshCubeOut = new Mat();
    private ArrayList<MatOfPoint> findCountoursCubeOut = new ArrayList<MatOfPoint>();
    private ArrayList<MatOfPoint> filteredCubeContours = new ArrayList<MatOfPoint>();
    private ArrayList<Rect> boundingBoxesCube = new ArrayList<Rect>();

    private Mat hsvThreshBallOut = new Mat();
    private ArrayList<MatOfPoint> findCountoursBallOut = new ArrayList<MatOfPoint>();
    private ArrayList<MatOfPoint> filteredBallContours = new ArrayList<MatOfPoint>();
    private ArrayList<Rect> boundingBoxesBall = new ArrayList<Rect>();

    private Mat processedImage;

    private int imgWidth = 864;
    private int imgHeight = 480;

    private int blurRadius = 2;

    private double[] cubeHsvHue = {10, 50};
    private double[] cubeHsvSaturation = {150, 255};
    private double[] cubeHsvMinValue = {110, 255};

    private double[] ballHsvHue = {0, 180};
    private double[] ballHsvSaturation = {0, 25};
    private double[] ballHsvMinValue = {220, 255};

    private int cubeMinHeight = 0;
    private int cubeMaxHeight = 100;
    private int cubeMinWidth = 10;
    private int cubeMaxWidth = 60;
    private int cubeMinArea = 50;

    private int ballMinHeight = 0;
    private int ballMaxHeight = 100;
    private int ballMinWidth = 10;
    private int ballMaxWidth = 60;
    private int ballMinArea = 50;

    public CubeSampler (Speed speed) {
        super();
        moduleName = "CubeSampler";
        sampleSpeed = speed;
        if (moduleLoggingEnabled) {
            op.debugLog.addDbgMessage(
                    BotcatsLog.DbgLevel.INFO,
                    moduleName,
                    "Initialized"
            );
        }
    }

    public CubeSampler () { }

    private void process(Mat image) {

        boundingBoxesCube.clear();
        boundingBoxesBall.clear();
        filteredCubeContours.clear();

        Mat resizeIn = image;
        Imgproc.pyrDown(resizeIn, resizeOut);
        if (sampleSpeed == SLOW) Imgproc.pyrDown(resizeOut, resizeOut);
        Mat blurIn = resizeOut;
        Imgproc.blur(blurIn, blurOut, new Size(blurRadius, blurRadius));
        Mat colorCvtIn = blurOut;
        Imgproc.cvtColor(colorCvtIn, colorCvtOut, Imgproc.COLOR_RGB2HSV_FULL);
        Mat hsvThreshCubeIn = colorCvtOut;
        Core.inRange(
                hsvThreshCubeIn,
                new Scalar(cubeHsvHue[0], cubeHsvSaturation[0], cubeHsvMinValue[0]),
                new Scalar(cubeHsvHue[1], cubeHsvSaturation[1], cubeHsvMinValue[1]),
                hsvThreshCubeOut
        );
        Mat findContoursCubeIn = hsvThreshCubeOut;
        Mat contourHierarchy = new Mat();
        Imgproc.findContours(findContoursCubeIn, findCountoursCubeOut, contourHierarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
        ArrayList<MatOfPoint> filterCubeIn = findCountoursCubeOut;

        filteredCubeContours.clear();
        for (MatOfPoint contour : filterCubeIn) {
            if (isGoodCube(contour)) {
                Core.multiply(contour, new Scalar(4.0, 4.0), contour);
                filteredCubeContours.add(contour);
            }
        }

        if (filteredCubeContours.size() > 1) {
            double largestSize = 0;
            for (int i = 0; i < filteredCubeContours.size(); i++) {
                if (Imgproc.contourArea(filteredCubeContours.get(i)) > largestSize) {
                    largestSize = Imgproc.contourArea(filteredCubeContours.get(i));
                    filteredCubeContours.add(0, filteredCubeContours.get(i));
                    filteredCubeContours.remove(i + 1);
                }
            }
        }

        for (MatOfPoint contour : filteredCubeContours) {
            Rect box = Imgproc.boundingRect(contour);
            boundingBoxesCube.add(box);
        }

        Mat hsvThreshBallIn = colorCvtOut;
        Core.inRange(
                hsvThreshBallIn,
                new Scalar(ballHsvHue[0], ballHsvSaturation[0], ballHsvMinValue[0]),
                new Scalar(ballHsvHue[1], ballHsvSaturation[1], ballHsvMinValue[1]),
                hsvThreshBallOut
        );
        Mat findContoursBallIn = hsvThreshBallOut;
        Imgproc.findContours(findContoursBallIn, findCountoursBallOut, contourHierarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
        ArrayList<MatOfPoint> filterBallIn = findCountoursBallOut;

        filteredBallContours.clear();
        for (MatOfPoint contour : filterBallIn) {
            if (isGoodBall(contour)) {
                Core.multiply(contour, new Scalar(2.0, 2.0), contour);
                if (sampleSpeed == SLOW) Core.multiply(contour, new Scalar(2.0, 2.0), contour);
                filteredBallContours.add(contour);
            }
        }

        for (MatOfPoint contour : filteredBallContours) {
            Rect box = Imgproc.boundingRect(contour);
            boundingBoxesBall.add(box);
        }

        processedImage = image;

        drawboundingBoxes(processedImage, boundingBoxesCube, new Scalar(255, 0 , 0));
        drawboundingBoxes(processedImage, boundingBoxesBall, new Scalar(0, 0 , 255));
        if (boundingBoxesCube.size() > 0) {
            Imgproc.putText(processedImage, getCubePosition().toString(), new Point(boundingBoxesCube.get(0).x, boundingBoxesCube.get(0).y), 0, 1, new Scalar(255, 255, 255));
        }
    }

    private void drawboundingBoxes(Mat image, ArrayList<Rect> boxes, Scalar color) {
        for (Rect box : boxes) {
            Imgproc.rectangle(
                    image,
                    new Point(box.x, box.y),
                    new Point(box.x + box.width, box.y + box.height),
                    color,
                    2
            );
        }
    }

    private boolean isGoodCube(MatOfPoint contour) {
        Rect boundBox = Imgproc.boundingRect(contour);
        if (boundBox.height < cubeMinHeight || boundBox.height > cubeMaxHeight)
            return false;
        if (boundBox.width < cubeMinWidth || boundBox.width > cubeMaxWidth)
            return false;
        if (Imgproc.contourArea(contour) < cubeMinArea)
            return false;

        return true;
    }

    boolean isGoodBall(MatOfPoint contour) {
        Rect boundBox = Imgproc.boundingRect(contour);
        if (boundBox.height < ballMinHeight || boundBox.height > ballMaxHeight)
            return false;
        if (boundBox.width < ballMinWidth || boundBox.width > ballMaxWidth)
            return false;
        if (Imgproc.contourArea(contour) < ballMinArea)
            return false;

        return true;
    }

    public Autonomous.CubePosition getCubePosition() {
        // Temporary thing until I can test detection on the silver minerals

        int cubeXPos = 0;
        int ballXpos1 = 0;
        int ballXpos2 = 0;


        if (boundingBoxesCube.size() > 0) {
            cubeXPos = boundingBoxesCube.get(0).x;

            if (boundingBoxesBall.size() == 2) {
                ballXpos1 = boundingBoxesBall.get(0).x;
                ballXpos2 = boundingBoxesBall.get(1).x;

                if (cubeXPos < ballXpos1 && cubeXPos < ballXpos2) {
                    return Autonomous.CubePosition.LEFT;
                } else if (cubeXPos > ballXpos1 && cubeXPos > ballXpos2) {
                    return Autonomous.CubePosition.RIGHT;
                } else {
                    return Autonomous.CubePosition.CENTER;
                }
            } else {
                if (cubeXPos < (imgWidth / 3) - 10) {
                    return Autonomous.CubePosition.LEFT;
                } else if (cubeXPos > ((2 * imgWidth) / 3) - 20) {
                    return Autonomous.CubePosition.RIGHT;
                } else {
                    return Autonomous.CubePosition.CENTER;
                }
            }
        }

        else {
            if (boundingBoxesBall.size() == 2) {
                ballXpos1 = boundingBoxesBall.get(0).x;
                ballXpos2 = boundingBoxesBall.get(1).x;
                if (ballXpos1 < ((2 * imgWidth) / 3) - 20 && ballXpos2 < ((2 * imgWidth) / 3) - 20) {
                    return Autonomous.CubePosition.RIGHT;
                } else if (ballXpos1 > (imgWidth / 3) - 10 && ballXpos2 > (imgWidth / 3) - 10) {
                    return Autonomous.CubePosition.LEFT;
                } else {
                    return Autonomous.CubePosition.CENTER;
                }
            } else return Autonomous.CubePosition.NONE;
        }
    }

    @Override
    public void onCameraViewStarted(int width, int height) {
        mRgba = new Mat(480, 864, CvType.CV_8UC4);
    }

    @Override
    public void onCameraViewStopped() {
        mRgba.release();
    }

    @Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        mRgba = inputFrame.rgba();
        filteredCubeContours.clear();
        findCountoursCubeOut.clear();
        boundingBoxesCube.clear();
        filteredBallContours.clear();
        findCountoursBallOut.clear();
        boundingBoxesBall.clear();
        process(mRgba);
        return processedImage;
    }

    public enum Speed {
        FAST,
        SLOW
    }

    private Speed sampleSpeed;
}
