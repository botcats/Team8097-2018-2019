package org.firstinspires.ftc.teamcode.hardware;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;

import org.firstinspires.ftc.teamcode.BaseOpMode;
import org.firstinspires.ftc.teamcode.utils.BotcatsLog;

import java.util.ArrayList;

public class MecanumDrive extends Drive {

    private DcMotor motorFL, motorFR, motorBL, motorBR;

    public MecanumDrive(DcMotor motorFrontLeft, DcMotor motorFrontRight, DcMotor motorBackLeft, DcMotor motorBackRight) {
        super();

        this.motorFL = motorFrontLeft;
        this.motorFR = motorFrontRight;
        this.motorBL = motorBackLeft;
        this.motorBR = motorBackRight;
        moduleName = "Mecanum Drive";

        if (op.imuEnabled) {
            imu = BaseOpMode.getInstance().imu;
        }

        if (op.coordinatesEnabled) {
            robotCoords = BaseOpMode.getInstance().robotCoords;
        }

        wheelCirc = HardwareConstants.CIRC_MECANUM_STANDARD; //Defaults to the circumference of standard mecanum wheels.

        leftMotors = new ArrayList<>();
        rightMotors = new ArrayList<>();
        allMotors = new ArrayList<>();

        leftMotors.add(motorFL);
        leftMotors.add(motorBL);

        rightMotors.add(motorFR);
        rightMotors.add(motorBR);

        allMotors.addAll(leftMotors);
        allMotors.addAll(rightMotors);

        for (DcMotor motor : allMotors) {
            motor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        }

        setDirection(leftMotors, DcMotorSimple.Direction.REVERSE);

        resetEncoders(allMotors);

        if (moduleLoggingEnabled) {
            op.debugLog.addDbgMessage(
                    BotcatsLog.DbgLevel.INFO,
                    moduleName,
                    "Initialized"
            );
        }
    }

    public void strafeLeft(double power) {
        motorBL.setPower(speed(power));
        motorBR.setPower(-speed(power));
        motorFL.setPower(-speed(power));
        motorFR.setPower(speed(power));
    }

    public void strafeRight(double power) {
        motorBL.setPower(-speed(power));
        motorBR.setPower(speed(power));
        motorFL.setPower(speed(power));
        motorFR.setPower(-speed(power));
    }

    public void strafeForwardRight(double power) {
        motorBL.setPower(0);
        motorBR.setPower(speed(power));
        motorFL.setPower(speed(power));
        motorFR.setPower(0);
    }

    public void strafeForwardLeft(double power) {
        motorBL.setPower(speed(power));
        motorBR.setPower(0);
        motorFL.setPower(0);
        motorFR.setPower(speed(power));
    }

    public void strafeBackwardRight(double targetPower) {

        motorBL.setPower(-speed(targetPower));
        motorBR.setPower(0);
        motorFL.setPower(0);
        motorFR.setPower(-speed(targetPower));
    }

    public void strafeBackwardLeft(double targetPower) {
        motorBL.setPower(0);
        motorBR.setPower(-speed(targetPower));
        motorFL.setPower(-speed(targetPower));
        motorFR.setPower(0);
    }
}
