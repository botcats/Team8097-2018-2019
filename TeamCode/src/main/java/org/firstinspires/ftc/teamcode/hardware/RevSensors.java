package org.firstinspires.ftc.teamcode.hardware;

import com.qualcomm.hardware.lynx.LynxModule;
import com.qualcomm.hardware.lynx.LynxNackException;
import com.qualcomm.hardware.lynx.commands.core.LynxGetADCCommand;
import com.qualcomm.hardware.lynx.commands.core.LynxGetADCResponse;

import org.firstinspires.ftc.teamcode.utils.BotcatsLog;
import org.firstinspires.ftc.teamcode.utils.Module;

public class RevSensors extends Module{
    LynxModule revHub;

    public RevSensors(LynxModule hub) {
        super();
        this.revHub = hub;

        moduleName = "Rev Hub";

        if (revHub != null) {

            if (moduleLoggingEnabled) {
                op.debugLog.addDbgMessage(
                        BotcatsLog.DbgLevel.INFO,
                        moduleName,
                        "Initialized"
                );
            }

            op.modules.add(this);

        } else {
            if (moduleLoggingEnabled) {
                op.debugLog.addDbgMessage(
                        BotcatsLog.DbgLevel.INFO,
                        moduleName,
                        "Not Found"
                );
            }
            op.revSensorsEnabled = false;
        }
    }

    public RevSensors() { }

    public double getMotorDraw(int motorPort) {
        LynxGetADCCommand.Channel channel;


        if (motorPort == 0) channel = LynxGetADCCommand.Channel.MOTOR0_CURRENT;
        else if (motorPort == 1) channel = LynxGetADCCommand.Channel.MOTOR1_CURRENT;
        else if (motorPort == 2) channel = LynxGetADCCommand.Channel.MOTOR2_CURRENT;
        else if (motorPort == 3) channel = LynxGetADCCommand.Channel.MOTOR3_CURRENT;
        else {
            if (moduleLoggingEnabled) {
                op.debugLog.addDbgMessage(
                        BotcatsLog.DbgLevel.WARN,
                        moduleName,
                        "Requested voltage of invalid port!"
                );
            }
            return 0;
        }

        LynxGetADCCommand command = new LynxGetADCCommand(revHub, channel, LynxGetADCCommand.Mode.ENGINEERING);
        try {
            LynxGetADCResponse response = command.sendReceive();
            return response.getValue();
        } catch (InterruptedException | LynxNackException e) {
            if (moduleLoggingEnabled) {
                op.debugLog.addDbgMessage(
                        BotcatsLog.DbgLevel.ERROR,
                        moduleName,
                        "Failed to get current for motor port " + motorPort
                );
            }
        }
        return 0;
    }

    public double getBatteryVoltage() {
        LynxGetADCCommand.Channel channel = LynxGetADCCommand.Channel.BATTERY_MONITOR;
        LynxGetADCCommand command = new LynxGetADCCommand(revHub, channel, LynxGetADCCommand.Mode.ENGINEERING);
        try {
            LynxGetADCResponse response = command.sendReceive();
            return response.getValue();
        } catch (InterruptedException | LynxNackException e) {
            if (moduleLoggingEnabled) {
                op.debugLog.addDbgMessage(
                        BotcatsLog.DbgLevel.ERROR,
                        moduleName,
                        "Failed to get voltage!"
                );
            }
        }
        return 0;
    }
}
